import nodemailer from 'nodemailer';

export default class EmailHelper {
    static async sendMail(to: string, subject: string, text: string, html?: string, file?: string) {
        const transporter = nodemailer.createTransport({
            host: process.env.MAIL_HOST,
            port: parseInt(process.env.MAIL_PORT),
            secure: true,
            auth: {
                user: process.env.MAIL_USER,
                pass: process.env.MAIL_PASSWORD,
            },
        });
        await transporter.verify();
        await transporter.sendMail({
            to,
            subject,
            text,
            html,
            attachments: [{ filename: file }]
        });
    }
}
