import api from './api';

const PORT = process.env.PORT || 3000;
const HOST = '0.0.0.0';

const server = api.listen(PORT, () => console.log(`Running on http://${HOST}:${PORT}`));

function terminate(server: { close: (arg0: (code: number) => void) => void }, options = { coredump: false, timeout: 500 }) {
	const exit = (code: number) => (options.coredump ? process.abort() : process.exit(code));
	return (_code: number, _reason: any) => (err: { message: any; stack: any }, _promise: any) => {
		if (err && err instanceof Error) console.log(err.message, err.stack);
		server.close(exit);
		// @ts-ignore
		setTimeout(exit, options.timeout).unref();
	};
}

// @ts-ignore
const exitHandler = terminate(server, {
	coredump: false,
	timeout: 500,
});

process.on('uncaughtException', exitHandler(1, 'Unexpected Error'));
process.on('unhandledRejection', exitHandler(1, 'Unhandled Promise'));
process.on('SIGTERM', exitHandler(0, 'SIGTERM'));
process.on('SIGINT', exitHandler(0, 'SIGINT'));
