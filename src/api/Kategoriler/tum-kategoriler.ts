import { Request, Response } from 'express';
import ApiResponseHandler from '../responseHandler';
import KategorilerService from '../../Services/Kategoriler';

export default async function (req: Request, res: Response) {
    ApiResponseHandler.send(res, await new KategorilerService(req).tumKategorileriGetir());
}
