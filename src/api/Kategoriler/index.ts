import { Router } from 'express';

export default function (app: Router) {
    app.get('/kategoriler', require('./tum-kategoriler').default);
}