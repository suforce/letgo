import { Request, Response } from 'express';
import UrunlerService from '../../Services/Urunler';
import ApiResponseHandler from '../responseHandler';

export default async function (req: Request, res: Response) {
    await ApiResponseHandler.send(res, await new UrunlerService(req).satistanUrunKaldir(req.query.urunId.toString()))
}