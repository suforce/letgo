import { Request, Response } from 'express';
import UrunlerService from '../../Services/Urunler';
import ApiResponseHandler from '../responseHandler';

export default async function (req: Request, res: Response) {
    ApiResponseHandler.send(res, await new UrunlerService(req).satisaUrunEkle(req.body.data));
}
