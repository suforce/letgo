import { Router } from 'express';
import validate from '../../validations';
import validationRules from '../../validations/Urunler';

export default function (app: Router) {
    app.post('/satisa-urun-ekle', validate(validationRules.satisaUrunEkle), require('./satisa-urun-ekle').default);
    app.delete('/satistan-urun-kaldir', validate(validationRules.satistanUrunKaldir), require('./satistan-urun-kaldir').default);
}
