import { Response } from 'express';

export default class ApiResponseHandler {
    static async send(res: Response, payload: { sonuc?: boolean, mesaj: string | [] | {} }) {
        res.status(payload.sonuc ? 200 : 403).send(payload.mesaj);
    }
}
