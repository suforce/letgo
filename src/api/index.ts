import cors from 'cors';
import path from 'path';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import { authMiddleware } from '../middlewares/authMiddleware';
import express, { NextFunction, Request, Response } from 'express';

const app = express();
app.use('/images', express.static(path.join(__dirname, '..', '..', 'Images')));
app.use(express.json({ limit: '100mb' }));
app.use(express.urlencoded({ limit: '100mb', extended: true, parameterLimit: 1000000 }));
app.use(cors({ origin: true }));
app.use(helmet());
app.use(bodyParser.json());

app.use(authMiddleware);
const routes = express.Router();


app.use('/api', routes);

app.get('/', (req: Request, res: Response) => res.send('Böyle birşeyin mümkün olmayacağını biliyor olman gerek.'));
app.all('*', (req: Request, res: Response, next: NextFunction) =>
	res.send('Buraya kadar boşuna geldin, çünkü yanlış yol.'),
);

export default app;
