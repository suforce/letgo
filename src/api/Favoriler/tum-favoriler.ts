import { Request, Response } from 'express';
import ApiResponseHandler from '../responseHandler';
import FavorilerService from '../../Services/Favoriler';

export default async function (req: Request, res: Response) {
    await ApiResponseHandler.send(res, await new FavorilerService(req).kullanicininTumFavorileriniGetir());
}
