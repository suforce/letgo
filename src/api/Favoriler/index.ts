import { Router } from 'express';
import validate from '../../validations';
import validationRules from '../../validations/Favoriler';

export default function (app: Router) {
    app.get('/tum-favoriler', require('./tum-favoriler').default);
    app.post('/favori-ekle', validate(validationRules.favoriEkle), require('./favori-ekle').default);
    app.delete('/favori-cikar', validate(validationRules.favoriCikar), require('./favori-cikar').default);
}
