import { Request, Response } from 'express';
import FavorilerService from '../../Services/Favoriler';
import ApiResponseHandler from '../responseHandler';

export default async function (req: Request, res: Response) {
    ApiResponseHandler.send(res, await new FavorilerService(req).favoriyeEkle(req.body.urunId));
}
