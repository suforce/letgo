import { Request, Response } from 'express';
import ApiResponseHandler from '../responseHandler';
import AltKategorilerService from '../../Services/AltKategoriler';

export default async function (req: Request, res: Response) {
    await ApiResponseHandler.send(res, await new AltKategorilerService(req).kategorininAltKategorileriniGetir(req.query.kategoriId.toString()));
}
