import { Router } from 'express';
import validate from '../../validations';
import validationRules from '../../validations/AltKategoriler';

export default function (app: Router) {
    app.get('/tum-alt-kategoriler', require('./tum-alt-kategoriler').default);
    app.get('/kategori-alt-kategorileri', validate(validationRules.kategoriAltKategorileriniGetir), require('./kategori-alt-kategorileri').default);
}
