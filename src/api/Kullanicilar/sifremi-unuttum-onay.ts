import { Request, Response } from 'express';
import ApiResponseHandler from '../responseHandler';
import KullanicilarService from '../../Services/Kullanicilar';

export default async function (req: Request, res: Response) {
    ApiResponseHandler.send(res, await new KullanicilarService(req).sifremiUnuttumOnay(req.body.passwordResetToken, req.body.sifre));
}
