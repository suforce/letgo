import { Router } from 'express';
import validate from '../../validations';
import validationRules from '../../validations/Kullanicilar';

export default function (app: Router) {
    app.post('/kayit-ol', validate(validationRules.kayitOl), require('./kayit-ol').default);
    app.post('/giris-yap', validate(validationRules.girisYap), require('./giris-yap').default);
    app.post('/kullanici-bilgilerini-duzenle', require('./kullanici-bilgilerini-duzenle').default);
    app.post('/sifremi-unuttum', validate(validationRules.sifremiUnuttum), require('./sifremi-unuttum').default);
    app.post('/sifremi-unuttum-onay', validate(validationRules.sifremiUnuttumOnay), require('./sifremi-unuttum-onay').default);
}
