import { Request, Response } from 'express';
import ApiResponseHandler from '../responseHandler';
import YorumlarService from '../../Services/Yorumlar';

export default async function (req: Request, res: Response) {
    await ApiResponseHandler.send(res, await new YorumlarService(req).uruneYorumEkle(req.body.data));
}