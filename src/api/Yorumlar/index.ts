import { Router } from 'express';
import validate from '../../validations';
import validationRules from '../../validations/Yorumlar';

export default function (app: Router) {
    app.post('/urune-yorum-ekle', validate(validationRules.uruneYorumEkle), require('./urune-yorum-ekle').default);
    app.delete('/urunden-yorum-kaldir', validate(validationRules.urundenYorumKaldir), require('./urunden-yorum-kaldir').default);
}
