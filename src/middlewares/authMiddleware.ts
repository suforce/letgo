import jwt from 'jsonwebtoken';
import WildcardMatch from 'wildcard-match';
import { NextFunction, Request, Response } from 'express';
// import KullanicilarService from '../Services/Kullanicilar';

export async function authMiddleware(req: Request, res: Response, next: NextFunction) {
	const authHeader = req.headers.authorization;
	// if (!authHeader) return res.status(401).send('Bearer Token Nerde Looo');
	const token = authHeader && authHeader.split(' ')[1];
	const isExceptedEndpoints = WildcardMatch([
		'/',
		'/api/sifremi-unuttum',
		'/api/giris-yap',
		'/api/kayit-ol',
		'/api/kullanici-bilgileri-bearer',
	]);
	if (isExceptedEndpoints(req.path)) return next();
	if (!token) return res.status(401).send('Bearer Token Nerde Looo');
	try {
		const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);
		// const user = await KullanicilarService.kullaniciBul(decoded['id']);
		// if (user) {
		// 	req['aktifKullanici'] = user;
		// 	return next();
		// } else res.status(401).send('Bearer Token Nerde Looo');
	} catch (error: unknown) {
		res.status(401).send('Bearer Token Nerde Looo');
	}
}
