import IService from './IService';
import Favoriler from '../database/model/favoriler';
import Kullanicilar from '../database/model/kullanicilar';

export default class FavorilerService extends IService {
    async favoriyeEkle(urunId: string) {
        await Favoriler.create({
            kullaniciId: this.aktifKullanici,
            urunId
        });
        return { sonuc: true, mesaj: 'Ürün Favoriye Eklendi !' };
    }

    async favoridenCikar(urunId: string) {
        await Favoriler.destroy({ where: { urunId, kullaniciId: this.aktifKullanici }, force: true });
        return { sonuc: true, mesaj: 'Ürün Favoriden Çıkarıldı !' };
    }

    async kullanicininTumFavorileriniGetir() {
        return {
            sonuc: true,
            mesaj: await Favoriler.findAll({
                where: { kullaniciId: this.aktifKullanici },
                include: [{ model: Kullanicilar, as: 'kullanicilar' }],
                raw: true,
            })
        };
    }
}
