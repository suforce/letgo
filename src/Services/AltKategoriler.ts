import IService from './IService';
import Kategoriler from '../database/model/kategoriler';
import AltKategoriler from '../database/model/altKategoriler';

export default class AltKategorilerService extends IService {
    async tumAltKategorileriGetir() {
        return {
            sonuc: true,
            mesaj: await AltKategoriler.findAll({ raw: true })
        };
    }

    async kategorininAltKategorileriniGetir(ustKategoriId: string) {
        return {
            sonuc: true,
            mesaj: await AltKategoriler.findAll({
                where: { ustKategoriId },
                include: [{ model: Kategoriler, as: 'kategoriler' }],
                raw: true
            })
        };
    }
}
