import Kategoriler from '../database/model/kategoriler';
import IService from './IService';

export default class KategorilerService extends IService {
    async tumKategorileriGetir() {
        return { sonuc: true, mesaj: await Kategoriler.findAll({ raw: true, plain: true }) };
    }
}
