import bcrypt from 'bcrypt';
import lodash from 'lodash';
import jwt from 'jsonwebtoken';
import { Op } from 'sequelize';
import IService from './IService';
import { v4 as uuidv4 } from 'uuid';
import EmailHelper from '../helpers/email';
import { sequelize } from '../database/model';
import Kullanicilar from '../database/model/kullanicilar';

export default class KullanicilarService extends IService {
    async girisYap(kullaniciAdi: string, sifre: string) {
        const kullanici = await Kullanicilar.findOne({
            raw: true,
            where: { [Op.or]: { kullaniciAdi, email: kullaniciAdi } }
        });
        if (kullanici) {
            if (bcrypt.compareSync(sifre, kullanici.getDataValue('sifre'))) {
                const sonuc = jwt.sign({ id: kullanici.getDataValue('id') }, String(process.env.JWT_SECRET_KEY), {
                    expiresIn: process.env.JWT_EXPIRES_IN
                });
                return { sonuc: true, mesaj: sonuc };
            }
        }
        return { sonuc: false, mesaj: 'Kullanıcı Adı Yada Şifre Hatalı !' };
    }

    async kayitOl(data: {
        adSoyad: string,
        kullaniciAdi: string,
        sifre: string,
        email: string
    }) {
        data.sifre = await bcrypt.hash(data.sifre, 12);
        const kullanici = await Kullanicilar.findOne({
            where: { [Op.or]: { kullaniciAdi: data.kullaniciAdi, email: data.email } },
        });
        if (kullanici) return { sonuc: false, mesaj: 'Bu Bilgilerle Kayıtlı Bir Kullanıcı Bulunmaktadır !' };
        const transaction = await sequelize.transaction();
        await Kullanicilar.create({
            adSoyad: data.adSoyad,
            email: data.email,
            sifre: data.sifre,
            kullaniciAdi: data.kullaniciAdi
        }, { transaction });
        await transaction.commit();
        return { sonuc: true, mesaj: 'Kayıt Başarılı !' };
    }

    async sifremiUnuttum(email: string) {
        const kullanici = await Kullanicilar.findOne({ where: { email } });
        if (!kullanici) return { sonuc: false, mesaj: 'Bu Bilgilerle Kayıt Bir Kullanıcı Bulunamadı' };
        const passwordResetToken = uuidv4().replace('-', '');
        await kullanici.update({ passwordResetToken });
        await EmailHelper.sendMail(email, 'Şifre Sıfırlama', '', '');
        return { sonuc: true, mesaj: 'Şifre Sıfırlama Maili Gönderildi !' };
    }

    async sifremiUnuttumOnay(passwordResetToken: string, sifre: string) {
        const kullanici = await Kullanicilar.findOne({ where: { passwordResetToken } });
        if (!kullanici) return { sonuc: false, mesaj: 'Şifre Sıfırlama Kodu Hatalı !' };
        await kullanici.update({ sifre });
        return { sonuc: true, mesaj: 'Şifre Başarıyla Değiştirildi !' };
    }

    async kullaniciBilgileriniDuzenle(data: { kullaniciAdi?: string; email?: string; adSoyad?: string; sifre?: string }) {
        const kullanici = await Kullanicilar.findByPk(this.aktifKullanici);
        if (bcrypt.compareSync(data.sifre, kullanici.getDataValue('sifre'))) {
            await kullanici.update({ ...lodash.pick(data, ['adSoyad', 'email', 'kullaniciAdi']) });
            return { sonuc: true, mesaj: 'Bilgiler Başarıyla Değiştirildi' };
        }
        return { sonuc: false, mesaj: 'Şifre Hatalı' };
    }
}
