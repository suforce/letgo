import IService from './IService';
import { sequelize } from '../database/model';
import Yorumlar from '../database/model/yorumlar';

export default class YorumlarService extends IService {
    async uruneYorumEkle(data: { urunId: string, yorum: string }) {
        const transaction = await sequelize.transaction();
        await Yorumlar.create({
            urunId: data.urunId,
            kullaniciId: this.aktifKullanici,
            yorum: data.yorum
        }, { transaction });

        return { sonuc: true, mesaj: 'Yorum Başarıyla Eklendi !' };
    }

    async urundenYorumSil(yorumId: string) {
        const transaction = await sequelize.transaction();
        const yorum = await Yorumlar.findByPk(yorumId, { transaction });
        if (!yorum) return { sonuc: false, mesaj: 'Yorum Bulunamadı !' };
        await yorum.destroy({ force: true, transaction });
        return { sonuc: true, mesaj: 'Yorum Başarıyla Silindi !' };
    }
}
