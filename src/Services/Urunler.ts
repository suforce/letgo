import IService from './IService';
import { sequelize } from '../database/model';
import Urunler from '../database/model/urunler';

export default class UrunlerService extends IService {
    async satisaUrunEkle(data: { urunFiyati: number, sinirsizStok: boolean, urunAdi: string, koordinat: string }) {
        const transaction = await sequelize.transaction();
        await Urunler.create({
            urunFiyati: data.urunFiyati,
            sinirsizStok: data.sinirsizStok,
            urunAdi: data.urunAdi,
            koordinat: data.koordinat
        }, { transaction });
        return { sonuc: true, mesaj: 'Ürün Başarıyla Eklendi !' };
    }

    async satistanUrunKaldir(urunId: string) {
        const transaction = await sequelize.transaction();
        await Urunler.destroy({ where: { urunId }, force: true, transaction });
        return { sonuc: true, mesaj: 'Ürün Başarıyla Kaldırıldı !' };
    }
}
