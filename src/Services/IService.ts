import { Request } from 'express';

export default abstract class IService {
	req: Request;
	protected aktifKullanici: string;

	constructor(req: Request) {
		this.req = req;
		this.aktifKullanici = req['aktifKullanici'].id;
	}
}
