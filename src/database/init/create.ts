import Urunler from '../model/urunler';
import Yorumlar from '../model/yorumlar';
import Favoriler from '../model/favoriler';
import Kategoriler from '../model/kategoriler';
import TicaretPuani from '../model/ticaretPuani';
import Kullanicilar from '../model/kullanicilar';
import AltKategoriler from '../model/altKategoriler';

async function tablolariKur() {
    await Kullanicilar.sync();
    await Urunler.sync();
    await Favoriler.sync();
    await Kategoriler.sync();
    await Yorumlar.sync();
    await AltKategoriler.sync();
    await TicaretPuani.sync();
}
tablolariKur();