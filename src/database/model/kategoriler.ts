import { sequelize } from '.';
import { Model, DataTypes } from 'sequelize';

export default class Kategoriler extends Model {}

Kategoriler.init(
	{
		id: {
			type: DataTypes.UUID,
			primaryKey: true,
			defaultValue: DataTypes.UUIDV4,
		},
		kategoriAdi: DataTypes.STRING({ length: 255 }),
	},
	{
		timestamps: false,
		sequelize,
		tableName: 'kategoriler',
	},
);
