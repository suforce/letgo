import { sequelize } from '.';
import Urunler from './urunler';
import Kullanicilar from './kullanicilar';
import { Model, DataTypes } from 'sequelize';

export default class Yorumlar extends Model { }
Yorumlar.init({
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
    },
    urunId: DataTypes.UUID,
    kullaniciId: DataTypes.UUID,
    yorum: DataTypes.TEXT
}, {
    sequelize,
    tableName: 'yorumlar',
    indexes: [{ fields: ['urunId', 'kullaniciId'] }]
});

Yorumlar.belongsTo(Urunler, { as: 'urunler' });
Yorumlar.belongsTo(Kullanicilar, { as: 'kullanicilar' });
