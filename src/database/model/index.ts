import dotenv from 'dotenv';
import { Sequelize } from 'sequelize';

dotenv.config();

export const sequelize = new Sequelize({
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT!),
    username: process.env.DATABASE_USERNAME,
    dialect: 'mysql',
    database: process.env.DATABASE_DATABASE,
    password: process.env.DATABASE_PASSWORD,
    logging: (log) => console.log(log),
    define: {
        charset: 'utf8',
        collate: 'utf8_turkish_ci',
    },
});
