import { sequelize } from '.';
import { Model, DataTypes } from 'sequelize';

export default class Kullanicilar extends Model { }
Kullanicilar.init(
    {
        id: {
            type: DataTypes.UUID,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
        },
        kullaniciAdi: DataTypes.STRING({ length: 100 }),
        adSoyad: DataTypes.STRING({ length: 255 }),
        passwordResetToken: DataTypes.STRING({ length: 32 }),
        sifre: {
            type: DataTypes.STRING({ length: 255 }),
            get: () => undefined
        },
        email: {
            type: DataTypes.STRING({ length: 255 }),
            validate: { isEmail: true },
        },
    },
    {
        sequelize,
        tableName: 'kullanicilar'
    }
);