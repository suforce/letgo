import { sequelize } from '.';
import { Model, DataTypes } from 'sequelize';
import Kategoriler from './kategoriler';

export default class AltKategoriler extends Model { }
AltKategoriler.init({
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
    },
    kategoriAdi: DataTypes.STRING,
    ustKategoriId: DataTypes.UUID
}, {
    sequelize,
    tableName: 'altKategoriler',
    indexes: [{ fields: ['ustKategoriId'] }]
});
AltKategoriler.belongsTo(Kategoriler, { as: 'kategoriler' });