import { sequelize } from '.';
import { Model, DataTypes } from 'sequelize';
import Kullanicilar from './kullanicilar';

export default class TicaretPuani extends Model { }
TicaretPuani.init({
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
    },
    alan: DataTypes.UUID,
    veren: DataTypes.UUID,
    yorum: DataTypes.TEXT,
    puan: DataTypes.BOOLEAN // true pozitif, false negatif
}, {
    sequelize,
    tableName: 'ticaretPuani',
    indexes: [{ fields: ['alan', 'veren'] }]
});

TicaretPuani.belongsTo(Kullanicilar, { as: 'kullanicilar' });