import { sequelize } from '.';
import { Model, DataTypes } from 'sequelize';
import Kullanicilar from './kullanicilar';

export default class Urunler extends Model { }
Urunler.init({
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
    },
    urunAdi: DataTypes.STRING,
    urunFiyati: DataTypes.DECIMAL(10, 2),
    koordinat: DataTypes.STRING({ length: 255 }),
    sinirsizStok: DataTypes.BOOLEAN,
    kullaniciId: DataTypes.UUID
}, {
    sequelize,
    tableName: 'urunler',
    indexes: [{ fields: ['kullaniciId'] }]
});
Urunler.belongsTo(Kullanicilar, { as: 'kullanicilar' });