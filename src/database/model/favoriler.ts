import { sequelize } from '.';
import Urunler from './urunler';
import Kullanicilar from './kullanicilar';
import { Model, DataTypes } from 'sequelize';

export default class Favoriler extends Model { }
Favoriler.init({
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
    },
    kullaniciId: DataTypes.UUID,
    urunId: DataTypes.UUID
}, {
    sequelize,
    tableName: 'favoriler',
    indexes: [{ fields: ['kullaniciId', 'urunId'] }]
});
Favoriler.belongsTo(Kullanicilar, { as: 'kullanicilar' });
Favoriler.belongsTo(Urunler, { as: 'urunler' });