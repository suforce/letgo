import { body, oneOf } from 'express-validator';

export default {
    girisYap: [body('kullaniciAdi').isString(), body('sifre').isString()],
    kayitOl: [
        body('data.adSoyad').isString(),
        body('data.kullaniciAdi').isString(),
        body('data.sifre').isString(),
        body('data.email').isEmail()
    ],
    sifremiUnuttum: [body('email').isString()],
    sifremiUnuttumOnay: [body('passwordResetToken').isString(), body('sifre').isString()],
}
