import { body, query } from 'express-validator';

export default {
    favoriEkle: [body('urunId').isUUID(4)],
    favoriCikar: [query('urunId').isUUID(4)],
}