import { query } from 'express-validator';

export default {
    kategoriAltKategorileriniGetir: [query('kategoriId').isUUID()]
}
