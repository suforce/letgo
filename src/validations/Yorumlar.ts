import { body, query } from 'express-validator';

export default {
    uruneYorumEkle: [
        body('data.urunId').isUUID(4),
        body('data.yorum').isString()
    ],
    urundenYorumKaldir: [query('yorumId').isUUID(4)]
}