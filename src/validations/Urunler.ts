import { body, query } from 'express-validator';

export default {
    satisaUrunEkle: [
        body('data.urunFiyati').isDecimal(),
        body('data.sinirsizStok').isBoolean(),
        body('data.urunAdi').isString(),
        body('data.koordinat').isString()
    ],
    satistanUrunKaldir: [query('urunId').isUUID(4)]
}