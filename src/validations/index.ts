import { Request, Response, NextFunction } from 'express';
import { validationResult, ValidationChain } from 'express-validator';

export default (validations: ValidationChain[]) => {
    return async (req: Request, res: Response, next: NextFunction) => {
        await Promise.all(validations.map((validation) => validation.run(req)));
        const errors = validationResult(req);
        if (errors.isEmpty()) return next();
        const mappedErrors = {};
        errors.array().forEach((value) => {
            if (mappedErrors.hasOwnProperty(value.param)) mappedErrors[value.param].push(value.msg);
            else mappedErrors[value.param] = [value.msg];
        });
        res.status(422).json({
            message: '422 Unprocessable Entity',
            errors: mappedErrors,
            status_code: 422,
        });
    };
};
